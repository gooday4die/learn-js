'use strict';

/*
 Формат JSON, метод toJSON

 JSON – формат для представления объектов (и не только) в виде строки.
 Методы JSON.parse и JSON.stringify позволяют интеллектуально преобразовать объект в строку и обратно.
 */

/*
 Задача 1: Превратите объект в JSON
 Превратите объект leader из примера ниже в JSON:

 var leader = {
 name: "Василий Иванович",
 age: 35
 };
 После этого прочитайте получившуюся строку обратно в объект.
 */

let leader = {
    name: "Василий Иванович",
    age: 35
};

leader = JSON.stringify(leader);

console.log(leader);

leader = JSON.parse(leader);

console.log(leader);

/*
Задача 2: Превратите объекты со ссылками в JSON
 Превратите объект team из примера ниже в JSON:

 var leader = {
 name: "Василий Иванович"
 };

 var soldier = {
 name: "Петька"
 };

 // эти объекты ссылаются друг на друга!
 leader.soldier = soldier;
 soldier.leader = leader;

 var team = [leader, soldier];
 Может ли это сделать прямой вызов JSON.stringify(team)? Если нет, то почему?
 Какой подход вы бы предложили для чтения и восстановления таких объектов?
 */

let leader2 = {
    name: "Василий Иванович",
    toJSON: function () {
        return this.name + ' ' + this.soldier.name;
    }
};

let soldier = {
    name: "Петька",
    toJSON: function () {
        return this.name + ' ' + this.leader.name;
    }
};

leader2.soldier = soldier;
soldier.leader = leader;

let team = [leader2, soldier];

// team = JSON.stringify(team);
// console.log(team);