'use strict';


/*
 Методы объектов, this
 */

/*
 Задача 1: Создайте калькулятор
 Создайте объект calculator с тремя методами:

 read() запрашивает prompt два значения и сохраняет их как свойства объекта
 sum() возвращает сумму этих двух значений
 mul() возвращает произведение этих двух значений
 var calculator = {
 ...ваш код...
 }

 calculator.read();
 alert( calculator.sum() );
 alert( calculator.mul() );
 */

let task1 = {
    firstValue: null,
    secondValue: null,

    read: function () {
        this.firstValue = +prompt('Первое число', '0');
        this.secondValue = +prompt('Второе число', '0');
    },

    sum: function () {
        return this.firstValue + this.secondValue;
    },

    mul: function () {
        return this.firstValue * this.secondValue;
    }
};

// task1.read();
// console.log( task1.sum() );
// console.log( task1.mul() );

/*
 Задача 2: Цепочка вызовов
 Есть объект «лестница» ladder:

 var ladder = {
 step: 0,
 up: function() { // вверх по лестнице
 this.step++;
 },
 down: function() { // вниз по лестнице
 this.step--;
 },
 showStep: function() { // вывести текущую ступеньку
 alert( this.step );
 }
 };
 Сейчас, если нужно последовательно вызвать несколько методов объекта, это можно сделать так:

 ladder.up();
 ladder.up();
 ladder.down();
 ladder.showStep(); // 1
 Модифицируйте код методов объекта, чтобы вызовы можно было делать цепочкой, вот так:

 ladder.up().up().down().up().down().showStep(); // 1
 Как видно, такая запись содержит «меньше букв» и может быть более наглядной.

 Такой подход называется «чейнинг» (chaining) и используется, например, во фреймворке jQuery.
 */

var task2 = {
    step: 0,
    up: function() {
        this.step++;
        return this;
    },
    down: function() {
        this.step--;
        return this;
    },
    showStep: function() {
        console.log( this.step );
        return this;
    }
};

//task2.up().up().down().up().down().showStep();

