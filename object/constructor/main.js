'use strict';

/*
 Создание объектов через "new"

 Объекты могут быть созданы при помощи функций-конструкторов:

 - Любая функция может быть вызвана с new, при этом она получает новый пустой объект в качестве this, в который она
 добавляет свойства. Если функция не решит возвратить свой объект, то её результатом будет this.
 - Функции, которые предназначены для создания объектов, называются конструкторами. Их названия пишут с большой буквы,
 чтобы отличать от обычных.
 */

/*
 Задача 1: Создать Calculator при помощи конструктора
 Напишите функцию-конструктор Calculator, которая создает объект с тремя методами:

 Метод read() запрашивает два значения при помощи prompt и запоминает их в свойствах объекта.
 Метод sum() возвращает сумму запомненных свойств.
 Метод mul() возвращает произведение запомненных свойств.
 Пример использования:

 var calculator = new Calculator();
 calculator.read();

 alert( "Сумма=" + calculator.sum() );
 alert( "Произведение=" + calculator.mul() );
 */

let Calcularot = function () {
    this.firstValue = null;
    this.secondValue = null;

    this.read = function () {
        this.firstValue = +prompt('Введите первое число: ');
        this.secondValue = +prompt('Введите второе число: ');
    };

    this.sum = () => this.firstValue + this.secondValue;
    this.mul = () => this.firstValue * this.secondValue;
};

let task1 = new Calcularot();
// task1.read();
// console.log("Сумма=" + task1.sum());
// console.log("Произведение=" + task1.mul());

/*
 Задача 2: Создать Accumulator при помощи конструктора
 Напишите функцию-конструктор Accumulator(startingValue). Объекты, которые она создает, должны хранить текущую сумму и прибавлять к ней то, что вводит посетитель.

 Более формально, объект должен:

 Хранить текущее значение в своём свойстве value. Начальное значение свойства value ставится конструктором равным startingValue.
 Метод read() вызывает prompt, принимает число и прибавляет его к свойству value.
 Таким образом, свойство value является текущей суммой всего, что ввел посетитель при вызовах метода read(), с учетом начального значения startingValue.

 Ниже вы можете посмотреть работу кода:

 var accumulator = new Accumulator(1); // начальное значение 1
 accumulator.read(); // прибавит ввод prompt к текущему значению
 accumulator.read(); // прибавит ввод prompt к текущему значению
 alert( accumulator.value ); // выведет текущее значение
 */

let Accumulator = function (startingValue) {
    this.value = +startingValue;

    this.read = () => this.value += +prompt('Введите число');
};

// let accumulator = new Accumulator(1);
// accumulator.read();
// accumulator.read();
// console.log(accumulator.value);

/*
 Задача 3: Создайте калькулятор
 Напишите конструктор Calculator, который создаёт расширяемые объекты-калькуляторы.

 Эта задача состоит из двух частей, которые можно решать одна за другой.

 Первый шаг задачи: вызов calculate(str) принимает строку, например «1 + 2», с жёстко заданным форматом
 «ЧИСЛО операция ЧИСЛО» (по одному пробелу вокруг операции), и возвращает результат. Понимает плюс + и минус -.

 Пример использования:

 var calc = new Calculator;

 alert( calc.calculate("3 + 7") ); // 10
 Второй шаг – добавить калькулятору метод addMethod(name, func), который учит калькулятор новой операции. Он получает
 имя операции name и функцию от двух аргументов func(a,b), которая должна её реализовывать.

 Например, добавим операции умножить *, поделить / и возвести в степень **:

 var powerCalc = new Calculator;
 powerCalc.addMethod("*", function(a, b) {
 return a * b;
 });
 powerCalc.addMethod("/", function(a, b) {
 return a / b;
 });
 powerCalc.addMethod("**", function(a, b) {
 return Math.pow(a, b);
 });

 var result = powerCalc.calculate("2 ** 3");
 alert( result ); // 8
 Поддержка скобок и сложных математических выражений в этой задаче не требуется.
 Числа и операции могут состоять из нескольких символов. Между ними ровно один пробел.
 Предусмотрите обработку ошибок. Какая она должна быть – решите сами.
 */

let Calculator2 = function () {
    this.artificialIntelligence = {
        '+': function (firstValue, secondValue) {
            return firstValue + secondValue;
        },
    };
    this.calculate = function (calculate) {
        let arrayCalculates = calculate.split(' ');
        if (!this.artificialIntelligence[arrayCalculates[1]]) {
            return 'Калькулятор еще не обучили данному методу!';
        }
        return this.artificialIntelligence[arrayCalculates[1]](parseInt(arrayCalculates[0]), parseInt(arrayCalculates[2]));
    };

    this.addMethod = function (name, func) {
        this.artificialIntelligence[name] = func;
    }
};

let powerCalc = new Calculator2();
powerCalc.addMethod("/", function (a, b) {
    return a / b;
});
// console.log(powerCalc.calculate('1 + 1'));
// console.log(powerCalc.calculate('1 test 1'));
// console.log(powerCalc.calculate('1a + 12'));
// console.log(powerCalc.calculate('4 / 2'));
