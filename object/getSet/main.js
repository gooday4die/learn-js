'use strict';

/*
 Дескрипторы, геттеры и сеттеры свойств
 Object.keys(obj), Object.getOwnPropertyNames(obj) - Возвращают массив – список свойств объекта.
 Object.getOwnPropertyDescriptor(obj, prop) - Возвращает дескриптор для свойства obj[prop].

 */

/*
 Задача 1: Добавить get/set-свойства
 Вам попал в руки код объекта User, который хранит имя и фамилию в свойстве this.fullName:

 function User(fullName) {
 this.fullName = fullName;
 }

 var vasya = new User("Василий Попкин");
 Имя и фамилия всегда разделяются пробелом.

 Сделайте, чтобы были доступны свойства firstName и lastName, причём не только на чтение, но и на запись, вот так:

 var vasya = new User("Василий Попкин");

 // чтение firstName/lastName
 alert( vasya.firstName ); // Василий
 alert( vasya.lastName ); // Попкин

 // запись в lastName
 vasya.lastName = 'Сидоров';

 alert( vasya.fullName ); // Василий Сидоров
 Важно: в этой задаче fullName должно остаться свойством, а firstName/lastName – реализованы через get/set. Лишнее дублирование здесь ни к чему.
 */

let User = function (fullname) {
    this.fullName = fullname;
    let _ = this;

    Object.defineProperty(this, 'firstName', {
        get: function () {
            let splitter = _.fullName.split(' ');
            return splitter[0] ? splitter[0] : "Нет имени";
        },
        set: function (value) {
            let splitter = _.fullName.split(' ');
            splitter[0] = value;
            return this.fullName = splitter.join(' ');
        }
    });

    Object.defineProperty(this, 'lastName', {
        get: function () {
            let splitter = _.fullName.split(' ');
            return splitter[1] ? splitter[1] : "Нет фамилии";
        },
        set: function (value) {
            let splitter = _.fullName.split(' ');
            splitter[1] = value;
            return this.fullName = splitter.join(' ');
        }
    });
};

let user = new User('Никита Лощенин');

// console.log(user.fullName);
// user.firstName = 'Вася';
// user.lastName = 'Пупкин';
// console.log(user.fullName);
// console.log(user);

