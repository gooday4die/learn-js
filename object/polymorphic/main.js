'use strict';

/*
 Типы данных: [[Class]], instanceof и утки

 Для написания полиморфных (это удобно!) функций нам нужна проверка типов.

 Для примитивов с ней отлично справляется оператор typeof.

 У него две особенности:

 Он считает null объектом, это внутренняя ошибка в языке.
 Для функций он возвращает function, по стандарту функция не считается базовым типом, но на практике это удобно и полезно.
 Для встроенных объектов мы можем получить тип из скрытого свойства [[Class]],
 при помощи вызова {}.toString.call(obj).slice(8, -1). Для конструкторов, которые объявлены нами, [[Class]]
 всегда равно "Object".

 Оператор obj instanceof Func проверяет, создан ли объект obj функцией Func, работает для любых конструкторов.
 Более подробно мы разберём его в главе Проверка класса: "instanceof".

 И, наконец, зачастую достаточно проверить не сам тип, а просто наличие нужных свойств или методов. Это называется
 «утиная типизация».

 */

/*
 Задача 1: Полиморфная функция formatDate

 Напишите функцию formatDate(date), которая возвращает дату в формате dd.mm.yy.

 Ее первый аргумент должен содержать дату в одном из видов:

 Как объект Date.
 Как строку, например yyyy-mm-dd или другую в стандартном формате даты.
 Как число секунд с 01.01.1970.
 Как массив [гггг, мм, дд], месяц начинается с нуля
 Для этого вам понадобится определить тип данных аргумента и, при необходимости, преобразовать входные данные в нужный формат.

 Пример работы:

 function formatDate(date) { // ваш код  }

 alert( formatDate('2011-10-02') ); // 02.10.11
 alert( formatDate(1234567890) ); // 14.02.09
 alert( formatDate([2014, 0, 1]) ); // 01.01.14
 alert( formatDate(new Date(2014, 0, 1)) ); // 01.01.14
 */

console.log(typeof('2011-10-02'), '2011-10-02');
console.log(typeof(1234567890), '1234567890');
console.log(typeof([2014, 0, 1]), '[2014, 0, 1]');
console.log(typeof(new Date(2014, 0, 1)), 'new Date(2014, 0, 1)');
console.log("___________________________________________________________");

let formatDate = (...values) => {

    let option = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    };

    if (values && values[0]) {
        console.log('тип : ' + typeof values[0]);
        if (typeof values[0] == 'string') {
            return stringValue(values[0])
        }
        if (typeof values[0] == 'number') {
            return numberValue(values[0])
        }
        if (values[0].forEach) {
            return arrayValue(values[0])
        }
        if ((typeof (values[0]) == 'object') && !values[0].foreach && (values[0] instanceof Date)) {
            return objectValue(values[0])
        }
    }

    function stringValue(string) {
        // По идее мы здесь приводим string к нормальному виду, но вот так вот...
        return new Date(string).toLocaleString("ru", option);
    }

    function numberValue(number) {
        // По идее мы здесь приводим string к нормальному виду, но вот так вот...
        return new Date(+number).toLocaleString("ru", option);
    }

    function arrayValue(array) {
        array[1]++;
        // По идее мы здесь приводим string к нормальному виду, но вот так вот...
        return new Date(array).toLocaleString("ru", option);
    }

    function objectValue(object) {
        // По идее мы здесь приводим string к нормальному виду, но вот так вот...
        return object.toLocaleString("ru", option);
    }

};

// console.log(formatDate('2011-10-02'));
// console.log(formatDate(1234567890));
// console.log(formatDate([2014, 0, 1]));
// console.log(formatDate(new Date(2014, 0, 1)));