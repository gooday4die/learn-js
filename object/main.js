"use strict";

// ЗАДАНИЕ 1
// function isEmpty(obj) {
//     var count = null;
//     for (var key in obj) {
//         count++;
//     }
//     return count ? false : true;
// }
//
// var schedule = {};
//
// // console.log( isEmpty(schedule) ); // true
//
// // schedule["8:30"] = "подъём";
//
// // console.log( isEmpty(schedule) ); // false

/* ЗАДАНИЕ 2 */
// var salaries = {
//     "Вася": 100,
//     "Петя": 300,
//     "Даша": 250
// };
//
// function summarizesSalary(object) {
//     let summ = 0;
//
//     for (var key in object) {
//         if (typeof object[key] == "number") {
//             summ += object[key];
//         }
//     }
//
//     return summ;
// }
//
// console.log(summarizesSalary(salaries));
//
// //... ваш код выведет 650

// ЗАДАНИЕ 3
// Есть объект salaries с зарплатами. Напишите код, который выведет имя сотрудника, у которого самая большая зарплата.
// Если объект пустой, то пусть он выводит «нет сотрудников».
// var salaries = {
//     "Вася": 100,
//     "Петя": 300,
//     "Даша": 250
// };
//
// function searchMaximumSalary(obj) {
//     let result = {
//         name: null,
//         salary : null,
//     };
//
//     for (let key in obj) {
//          if (obj[key] > result.salary) {
//              result.name = key;
//              result.salary = obj[key];
//          }
//     }
//     return result;
// }
//
// console.log(searchMaximumSalary(salaries));

// ЗАДАНИЕ 4
// var menu = {
//     width: 200,
//     height: 300,
//     title: "My menu"
// };
//
// function multiplyNumeric(object) {
//     _.each(object, function (element, key, list) {
//         if (_.isFinite(element)) {
//             list[key] = element * 2;
//         }
//     });
//     return object;
// }
//
// console.log(multiplyNumeric(menu));