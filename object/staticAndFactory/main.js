'use strict';

/*
 Статические и фабричные методы

 - Общие действия и подсчёты, имеющие отношения ко всем объектам данного типа.
 - Методы, не привязанные к конкретному объекту, например сравнение.
 - Вспомогательные методы, которые полезны вне объекта, например для форматирования даты.

 */

/*
 Задача 1: Счетчик объектов
 Добавить в конструктор Article:

 Подсчёт общего количества созданных объектов.
 Запоминание даты последнего созданного объекта.
 Используйте для этого статические свойства.

 Пусть вызов Article.showStats() выводит то и другое.

 Использование:




 function Article() {
 this.created = new Date();
 // ... ваш код ...
 }

 new Article();
 new Article();

 Article.showStats(); // Всего: 2, Последняя: (дата)

 new Article();

 Article.showStats(); // Всего: 3, Последняя: (дата)
 */

function Article() {
    this.created = new Date();
    Article.count++;
    Article.lastTime = new Date();

    Article.showStats = function () {
      return  console.log(Article.count, Article.lastTime);
    }
}

Article.count = 0;

new Article();
new Article();

Article.showStats();

new Article();

Article.showStats();
