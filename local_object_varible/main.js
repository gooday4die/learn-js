'use strict';

/*
 Локальные переменные для объекта

 Пример:
 function makeCounter() {
 var currentCount = 1;

 // возвращаемся к функции
 function counter() {
 return currentCount++;
 }

 // ...и добавляем ей методы!
 counter.set = function(value) {
 currentCount = value;
 };

 counter.reset = function() {
 currentCount = 1;
 };

 return counter;
 }

 var counter = makeCounter();
 */

/*
 Задача 1: Сумма через замыкание
 Напишите функцию sum, которая работает так: sum(a)(b) = a+b.

 Да, именно так, через двойные скобки (это не опечатка). Например:

 sum(1)(2) = 3
 sum(5)(-1) = 4
 */
function task1(a) {

    return function (b) {
        return a + b;
    };
}

// console.log( task1(1)(2) );
// console.log( task1(5)(-1) );

/*
 Задача 2: Функция - строковый буфер
 В некоторых языках программирования существует объект «строковый буфер», который аккумулирует внутри себя значения. Его функционал состоит из двух возможностей:

 Добавить значение в буфер.
 Получить текущее содержимое.
 Задача – реализовать строковый буфер на функциях в JavaScript, со следующим синтаксисом:

 Создание объекта: var buffer = makeBuffer();.
 Вызов makeBuffer должен возвращать такую функцию buffer, которая при вызове buffer(value) добавляет значение в некоторое внутреннее хранилище, а при вызове без аргументов buffer() – возвращает его.
 Вот пример работы:

 function makeBuffer() { // ваш код }

 var buffer = makeBuffer();

 // добавить значения к буферу
 buffer('Замыкания');
 buffer(' Использовать');
 buffer(' Нужно!');

 // получить текущее значение
 alert( buffer() ); // Замыкания Использовать Нужно!
 Буфер должен преобразовывать все данные к строковому типу:

 var buffer = makeBuffer();
 buffer(0);
 buffer(1);
 buffer(0);

 alert( buffer() ); // '010'
 Решение не должно использовать глобальные переменные.
 */
function task2(string) {
    let result = String();

    return function (string) {
        if (string === undefined) return result;
        string = String(string);
        result += string;
    }
}

let buffer = task2();
buffer(0);
buffer(1);
buffer(0);

console.log(buffer());

/*
 Задача 3: Строковый буфер с очисткой
 Добавьте буферу из решения задачи Функция - строковый буфер метод buffer.clear(), который будет очищать текущее содержимое буфера:

 function makeBuffer() {
 ...ваш код...
 }

 var buffer = makeBuffer();

 buffer("Тест");
 buffer(" тебя не съест ");
 alert( buffer() ); // Тест тебя не съест

 buffer.clear();

 alert( buffer() ); // ""
 */
function task3(string) {
    let result = String();

    counter.clear = function () {
        result = '';
    };

    function counter(string) {
        if (string === undefined) return result;
        string = String(string);
        result += string;
    }

    return counter;
}

let bufferClear = task3();
bufferClear(0);
bufferClear(1);
bufferClear(0);
console.log(bufferClear());


bufferClear.clear();

console.log(bufferClear());

/*
 Задача 4: Сортировка
 У нас есть массив объектов:

 var users = [{
 name: "Вася",
 surname: 'Иванов',
 age: 20
 }, {
 name: "Петя",
 surname: 'Чапаев',
 age: 25
 }, {
 name: "Маша",
 surname: 'Медведева',
 age: 18
 }];
 Обычно сортировка по нужному полю происходит так:

 // по полю name (Вася, Маша, Петя)
 users.sort(function(a, b) {
 return a.name > b.name ? 1 : -1;
 });

 // по полю age  (Маша, Вася, Петя)
 users.sort(function(a, b) {
 return a.age > b.age ? 1 : -1;
 });
 Мы хотели бы упростить синтаксис до одной строки, вот так:

 users.sort(byField('name'));
 users.forEach(function(user) {
 alert( user.name );
 }); // Вася, Маша, Петя

 users.sort(byField('age'));
 users.forEach(function(user) {
 alert( user.name );
 }); // Маша, Вася, Петя
 То есть, вместо того, чтобы каждый раз писать в sort function... – будем использовать byField(...)

 Напишите функцию byField(field), которую можно использовать в sort для сравнения объектов по полю field, чтобы пример выше заработал.
 */

let users = [{
    name: "Вася",
    surname: 'Иванов',
    age: 20
}, {
    name: "Петя",
    surname: 'Чапаев',
    age: 25
}, {
    name: "Маша",
    surname: 'Медведева',
    age: 18
}];

function task4(tag) {
    return function (a, b) {
        return a[tag] > b[tag] ? 1 : -1;
    }
}

// users.sort(task4('name'));
// users.forEach(function(user) {
//     console.log( user.name );
// });

/*
 Задача 5 : Фильтрация через функцию
 Создайте функцию filter(arr, func), которая получает массив arr и возвращает новый, в который входят только те элементы arr, для которых func возвращает true.
 Создайте набор «готовых фильтров»: inBetween(a,b) – «между a,b», inArray([...]) – "в массиве [...]". Использование должно быть таким:
 filter(arr, inBetween(3,6)) – выберет только числа от 3 до 6,
 filter(arr, inArray([1,2,3])) – выберет только элементы, совпадающие с одним из значений массива.
 Пример, как это должно работать:

 //.. ваш код для filter, inBetween, inArray
 var arr = [1, 2, 3, 4, 5, 6, 7];

 alert(filter(arr, function(a) {
 return a % 2 == 0
 })); // 2,4,6

 alert( filter(arr, inBetween(3, 6)) ); // 3,4,5,6

 alert( filter(arr, inArray([1, 2, 10])) ); // 1,2
 */
let arr = [1, 2, 3, 4, 5, 6, 7];

function inBetween(min, max) {
    return function (value) {
        return (value >= min) && (value <= max);
    };
}

function inArray(arrayComparison) {
    return function (value) {
        return (arrayComparison.indexOf(value) >= 0)
    }
}
function filter(arr, func) {
    let result = [];

    for (let i = 0; i < arr.length; i++) {

        func(arr[i]) ? result.push(arr[i]) : '';
    }

    return result;
}
// console.log(inBetween(3, 5));
// console.log(inArray([1, 2, 10]));

// console.log(filter(arr, inBetween(3, 6)));
// console.log(filter(arr, inArray([1,3,5,120])));

