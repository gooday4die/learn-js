"use strict";
/*
 1. Массивы с числовыми индексами
 */
// Задача 1
// Напишите код для получения последнего элемента goods.

let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function task1(array) {
    console.log(array[array.length - 1]);
}

// Задача 2
// У нас есть массив goods. Напишите код для добавления в его конец значения «Компьютер».

function task2(array) {
    array[array.length] = "Элемент добавлен в конец массива";
}

// Задача 3
// Создайте массив styles с элементами «Джаз», «Блюз».
// Добавьте в конец значение «Рок-н-Ролл»
// Замените предпоследнее значение с конца на «Классика». Код замены предпоследнего значения должен работать для массивов любой длины.
//  Удалите первое значение массива и выведите его alert.
//  Добавьте в начало значения «Рэп» и «Регги».

let styles = ["Джаз", "Блюз"];

function task3(array) {
    array[array.length] = "Рок-н-Ролл";
    console.log(array);
    array.length && array[array.length - 2] ? array[array.length - 2] = "Классика" : '';
    console.log(array.shift());
    array.unshift("Рэп", "Регги");
    console.log(array);
}

// Задача 4
// Напишите код для вывода alert случайного значения из массива:
let arr = ["Яблоко", "Апельсин", "Груша", "Лимон"];

function task4(array) {
    let rand = Math.floor(Math.random() * (array.length));
    return array[rand] ? array[rand] : "Что то пошло не так";
}

// Задача 5
/*
 Напишите код, который:

 Запрашивает по очереди значения при помощи prompt и сохраняет их в массиве.
 Заканчивает ввод, как только посетитель введёт пустую строку, не число или нажмёт «Отмена».
 При этом ноль 0 не должен заканчивать ввод, это разрешённое число.
 Выводит сумму всех значений массива
 */

function task5() {
    let array = [];

    while (true) {
        let varible = prompt("Введите число");

        if (varible === "" || varible === null || isNaN(varible)) {
            return calculated(array);
        }

        array.push(+varible);
    }

    function calculated(arr) {
        let result = 0;
        for (let i = 0; i < arr.length; i++) {
            result += arr[i];
        }
        return result;
    }
}

// Задача 6
/*
 Создайте функцию find(arr, value), которая ищет в массиве arr значение value и возвращает его номер, если найдено, или
 -1, если не найдено.
 */
let randomArray = ["test", 1, 2, NaN];

function task6(arr, value) {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            return i;
        }
        return -1;
    }
}

// Задача 7
/*
 Создайте функцию filterRange(arr, a, b), которая принимает массив чисел arr и возвращает новый массив, который содержит
 только числа из arr из диапазона от a до b. То есть, проверка имеет вид a ≤ arr[i] ≤ b. Функция не должна менять arr.
 */

let rangeArray = [5, 4, 3, 2, 1, 0, 8, 6, 7];

function task7(array, min, max) {
    let result = [];
    let range = checkMinMax(min, max);
    for (let i = 0; i < array.length; i++) {
        if ((_.isFinite(array[i])) && (range.min <= array[i]) && (array[i] <= range.max)) {
            result.push(array[i]);
        }
    }

    function checkMinMax(min, max) {
        if (min && max && min > max) {
            max += min;
            min = max - min;
            max -= min;
        }

        return {min: min, max: max};
    }

    return result;
}
/*
 2. Методы массивов

 push/pop, shift/unshift, splice – для добавления и удаления элементов.
 join/split – для преобразования строки в массив и обратно.
 slice – копирует участок массива.
 sort – для сортировки массива. Если не передать функцию сравнения – сортирует элементы как строки.
 reverse – меняет порядок элементов на обратный.
 concat – объединяет массивы.
 indexOf/lastIndexOf – возвращают позицию элемента в массиве (не поддерживается в IE8-).

 */

/*
 Задача 1
 Добавить класс в строку
 В объекте есть свойство className, которое содержит список «классов» – слов, разделенных пробелом:

 var obj = {
 className: 'open menu'
 }
 Создайте функцию addClass(obj, cls), которая добавляет в список класс cls, но только если его там еще нет:

 addClass(obj, 'new'); // obj.className='open menu new'
 addClass(obj, 'open'); // без изменений (класс уже существует)
 addClass(obj, 'me'); // obj.className='open menu new me'

 alert( obj.className ); // "open menu new me"
 P.S. Ваша функция не должна добавлять лишних пробелов.
 */

let classObj = {
    className: 'open menu',
};

function task21(obj, cls) {
    let classObjName = obj.className.split(' ');

    if (!(classObjName.indexOf(cls) >= 0)) {
        classObjName.push(cls);
        obj.className = classObjName.join(' ');
    }
}
/*
 task21(classObj, 'new'); // obj.className='open menu new'
 task21(classObj, 'open'); // без изменений (класс уже существует)
 task21(classObj, 'me'); // obj.className='open menu new me'

 console.log(classObj);
 */

// Задача 2
/*
 Перевести текст вида border-left-width в borderLeftWidth
 Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».

 То есть, дефисы удаляются, а все слова после них получают заглавную букву.

 Например:

 camelize("background-color") == 'backgroundColor';
 camelize("list-style-image") == 'listStyleImage';
 camelize("-webkit-transition") == 'WebkitTransition';
 Такая функция полезна при работе с CSS.

 P.S. Вам пригодятся методы строк charAt, split и toUpperCase.
 */

let string = "border-top-left-radius";

function task22(string) {
    let array = string.split('-');

    for (let i = 1; i < array.length; i++) {
        array[i] = array[i].charAt(0).toUpperCase() + array[i].slice(1);
        console.log(array[i][0]);
    }

    return array.join('');
}

// console.log(task22(string));

// Задача 3
/*
 Функция removeClass
 У объекта есть свойство className, которое хранит список «классов» – слов, разделенных пробелами:

 var obj = {
 className: 'open menu'
 };
 Напишите функцию removeClass(obj, cls), которая удаляет класс cls, если он есть:

 removeClass(obj, 'open'); // obj.className='menu'
 removeClass(obj, 'blabla'); // без изменений (нет такого класса)
 P.S. Дополнительное усложнение. Функция должна корректно обрабатывать дублирование класса в строке:

 obj = {
 className: 'my menu menu'
 };
 removeClass(obj, 'menu');
 alert( obj.className ); // 'my'
 Лишних пробелов после функции образовываться не должно.
 */

let classRemove = {
    className: 'open menu menu lol menu',
};

function task23(obj, cls) {
    let classObjName = obj.className.split(' '),
        index = classObjName.indexOf(cls);

    if (index >= 0) {
        classObjName.splice(index, 1);
        obj.className = classObjName.join(' ');
        task23(obj, cls);
    }

    return obj;
}

// console.log(task23(classRemove, 'menu'));

/*
 3. Массивы : перебераюшие методы

 forEach – для перебора массива.
 filter – для фильтрации массива.
 every/some – для проверки массива.
 map – для трансформации массива в массив.
 reduce/reduceRight – для прохода по массиву с вычислением значения.
 */

/*
 Задача 1:
 Перепишите цикл через map
 Код ниже получает из массива строк новый массив, содержащий их длины:

 var arr = ["Есть", "жизнь", "на", "Марсе"];

 var arrLength = [];
 for (var i = 0; i < arr.length; i++) {
 arrLength[i] = arr[i].length;
 }

 alert( arrLength ); // 4,5,2,5
 Перепишите выделенный участок: уберите цикл, используйте вместо него метод map.
 */
let lesson31Array = ["Есть", "жизнь", "на", "Марсе"];

function task31(arr) {
    return arr.map(function (string) {
        return string.length;
    });
}

// console.log(task31(lesson31Array));

/*
 Задача 2: Массив частичных сумм

 На входе массив чисел, например: arr = [1,2,3,4,5].

 Напишите функцию getSums(arr), которая возвращает массив его частичных сумм.

 Иначе говоря, вызов getSums(arr) должен возвращать новый массив из такого же числа элементов, в котором на каждой позиции должна быть сумма элементов arr до этой позиции включительно.

 То есть:

 для arr = [ 1, 2, 3, 4, 5 ]
 getSums( arr ) = [ 1, 1+2, 1+2+3, 1+2+3+4, 1+2+3+4+5 ] = [ 1, 3, 6, 10, 15 ]
 Еще пример: getSums([-2,-1,0,1]) = [-2,-3,-3,-2].

 Функция не должна модифицировать входной массив.
 В решении используйте метод arr.reduce.

 */

let lesson32Array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function task32(arr) {
    return arr.reduce(function (pre, elem, i, list) {
        return list[i] += pre;
    });
}

console.log(task32(lesson32Array));
