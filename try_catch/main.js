'use strict';

/*
 Перехват ошибок, "try..catch"

 Конструкция :
 try {

 // код ...

 } catch (err) {

 // обработка ошибки
 }

 - Конструкция try..catch..finally – она позволяет обработать произвольные ошибки в блоке кода.

 - Оператор throw err генерирует свою ошибку, в качестве err рекомендуется использовать объекты, совместимые с
 встроенным типом Error, содержащие свойства message и name.

 - Проброс исключения – catch(err) должен обрабатывать только те ошибки, которые мы рассчитываем в нём увидеть,
 остальные – пробрасывать дальше через throw err.

 - Оборачивание исключений – функция, в процессе работы которой возможны различные виды ошибок, может «обернуть их» в
 одну общую ошибку, специфичную для её задачи, и уже её пробросить дальше. Чтобы, при необходимости, можно было подробно
 определить, что произошло, исходную ошибку обычно присваивают в свойство этой, общей. Обычно это нужно для логирования.

 - В window.onerror можно присвоить функцию, которая выполнится при любой «выпавшей» из скрипта ошибке. Как правило,
 это используют в информационных целях, например отправляют информацию об ошибке на специальный сервис.
 */

/*
 Задача 1: Eval-калькулятор с ошибками
 Напишите интерфейс, который принимает математическое выражение (в prompt) и результат его вычисления через eval.

 При ошибке нужно выводить сообщение и просить переввести выражение.

 Ошибкой считается не только некорректное выражение, такое как 2+, но и выражение, возвращающее NaN, например 0/0.
 */
let Error = (e) => {
    this.name = e.name;
    this.message = e.message;
};

let errorCalculator = () => {
    let value = prompt("Введите математическое выражение");
    let result = null;

    try {
        result = eval(value);
        if (isNaN(result)) throw new TypeError("NaN результат");
    } catch (e) {
        console.error(e);
    } finally {
        console.log("Было выражение: " + value + "| Результат: " + result);
    }
};

errorCalculator();