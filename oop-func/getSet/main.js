'use strict';

/*
 ООП в функциональном стиле : Геттеры и сеттеры
 */

/*
 Задача 1: Написать объект с геттерами и сеттерами
 важность: 4решение
 Напишите конструктор User для создания объектов:

 С приватными свойствами имя firstName и фамилия surname.
 С сеттерами для этих свойств.
 С геттером getFullName(), который возвращает полное имя.
 Должен работать так:

 function User() {
 // ваш код
 }

 var user = new User();
 user.setFirstName("Петя");
 user.setSurname("Иванов");

 alert( user.getFullName() ); // Петя Иванов
 */

function User() {
    let firstName, surname;
    let _ = this;

    this.setFirstName = (value) => {
        firstName = value
    };
    this.setSurname = (value) => {
        surname = value
    };
    this.getFullName = () => `${firstName} ${surname}`;

};

let user = new User();
user.setFirstName("Петя");
user.setSurname("Иванов");

console.log(user.getFullName());

/*
 Задача 2: Добавить геттер для power
 Добавьте кофеварке геттер для приватного свойства power, чтобы внешний код мог узнать мощность кофеварки.

 Исходный код:

 function CoffeeMachine(power, capacity) {
 //...
 this.setWaterAmount = function(amount) {
 if (amount < 0) {
 throw new Error("Значение должно быть положительным");
 }
 if (amount > capacity) {
 throw new Error("Нельзя залить воды больше, чем " + capacity);
 }

 waterAmount = amount;
 };

 this.getWaterAmount = function() {
 return waterAmount;
 };

 }
 Обратим внимание, что ситуация, когда у свойства power есть геттер, но нет сеттера – вполне обычна.

 Здесь это означает, что мощность power можно указать лишь при создании кофеварки и в дальнейшем её можно прочитать, но нельзя изменить.
 */

function CoffeeMachine(power, capacity) {
    let waterAmount = 10;
    let _ = this;
    let WATER_HEAT_CAPACITY = 4200;
    let flag = false;

    function getTimeToBoil() {
        return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }

    this.setWaterAmount = function (amount) {
        if (amount < 0) {
            throw new Error("Значение должно быть положительным");
        }
        if (amount > capacity) {
            throw new Error("Нельзя залить больше, чем " + capacity);
        }

        waterAmount = amount;
    };

    function onReady() {
        alert('Кофе готов!');
    }

    this.run = function () {
        console.log(`В процессе... количесвто жидкоси для кипечения: ${waterAmount}`);
        toogleFlag(flag);
        setTimeout(function () {
            toogleFlag(flag);
            onReady();
        }, getTimeToBoil());
    };

    this.isRunning = function () {
        return `В даннвый момент ${flag ? 'запушен' : 'выключен'}!`;
    };

    let toogleFlag = function (tflag) {
        flag = !tflag;
    };

    this.getPower = () => power;

    this.addWater = (value) => {
        try {
            if (value < 0) {
                throw new Error("Значение должно быть положительным");
            }
            if ((waterAmount + value) > capacity) {
                throw new Error("Нельзя залить больше, чем " + capacity);
            }
            waterAmount += value;
        } catch (e) {
            console.error(e);
        }
        console.log(waterAmount);
    };

    this.getWaterAmount = function (amount) {
        return waterAmount;
    };

    this.setWaterAmount = function (amount) {
        if ((amount < 0) && (amount > capacity)) throw new Error(`Меньше 0 или больше ${capacity}`);

        waterAmount = amount;
    };

    this.setOnReady = function (func) {
        if (!arguments.length) return;

        onReady = func;
    };
}

let superTank = new CoffeeMachine(2000, 400);

// console.log(superTank.getPower());

/*
 Задача 3: Добавить публичный метод кофеварке
 Добавьте кофеварке публичный метод addWater(amount), который будет добавлять воду.

 При этом, конечно же, должны происходить все необходимые проверки – на положительность и превышение ёмкости.

 }
 Вот такой код должен приводить к ошибке:

 var coffeeMachine = new CoffeeMachine(100000, 400);

 */
// superTank.addWater(200);
// superTank.addWater(100);
// superTank.addWater(300); // Нельзя залить больше, чем 400
// superTank.run();

/*
 Задача 4: Создать сеттер для onReady
 важность: 5решение
 Обычно когда кофе готов, мы хотим что-то сделать, например выпить его.

 Сейчас при готовности срабатывает функция onReady, но она жёстко задана в коде:

 Создайте сеттер setOnReady, чтобы код снаружи мог назначить свой onReady, вот так:

 var coffeeMachine = new CoffeeMachine(20000, 500);
 coffeeMachine.setWaterAmount(150);

 coffeeMachine.setOnReady(function() {
 var amount = coffeeMachine.getWaterAmount();
 alert( 'Готов кофе: ' + amount + 'мл' ); // Кофе готов: 150 мл
 });

 coffeeMachine.run();
 P.S. Значение onReady по умолчанию должно быть таким же, как и раньше.

 P.P.S. Постарайтесь сделать так, чтобы setOnReady можно было вызвать не только до, но и после запуска кофеварки,
 то есть чтобы функцию onReady можно было изменить в любой момент до её срабатывания.
 */

// superTank.setOnReady(function() {
//     let amount = superTank.getWaterAmount();
//     alert( 'Готов кофе: ' + amount + 'мл' ); // Кофе готов: 150 мл
// });

// superTank.run();

/*
 Задача 5: Добавить метод isRunning
 важность: 5решение
 Из внешнего кода мы хотели бы иметь возможность понять – запущена кофеварка или нет.

 Для этого добавьте кофеварке публичный метод isRunning(), который будет возвращать true, если она запущена и false, если нет.

 Нужно, чтобы такой код работал:

 var coffeeMachine = new CoffeeMachine(20000, 500);
 coffeeMachine.setWaterAmount(100);

 alert( 'До: ' + coffeeMachine.isRunning() ); // До: false

 coffeeMachine.run();
 alert( 'В процессе: ' + coffeeMachine.isRunning() ); // В процессе: true

 coffeeMachine.setOnReady(function() {
 alert( "После: " + coffeeMachine.isRunning() ); // После: false
 });
 */

let coffeeMachine = new CoffeeMachine(20000, 500);
coffeeMachine.setWaterAmount(100);
alert('До: ' + coffeeMachine.isRunning());
coffeeMachine.run();
alert('В процессе: ' + coffeeMachine.isRunning()); // В процессе: true

coffeeMachine.setOnReady(function () {
    alert("После: " + coffeeMachine.isRunning()); // После: false
});