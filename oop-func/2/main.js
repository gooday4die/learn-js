'use strict';

/*
 ООП в функциональном стиле : Внутренний и внешний интерфейс

 - Защита пользователей от выстрела себе в ногу
 - Удобство в поддержке
 - При наличии чётко выделенного внешнего интерфейса, разработчик может свободно менять внутренние свойства и методы,
 без оглядки на коллег.
 -Управление сложностью
 -Всегда удобно, когда детали реализации скрыты, и доступен простой, понятно документированный внешний интерфейс.
 */

/*
 Задача 1: Добавить метод и свойство кофеварке
 важность: 5решение
 Улучшите готовый код кофеварки, который дан ниже: добавьте в кофеварку публичный метод stop(), который будет останавливать кипячение (через clearTimeout).

 function CoffeeMachine(power) {
 this.waterAmount = 0;

 var WATER_HEAT_CAPACITY = 4200;

 var self = this;

 function getBoilTime() {
 return self.waterAmount * WATER_HEAT_CAPACITY * 80 / power;
 }

 function onReady() {
 alert( 'Кофе готово!' );
 }

 this.run = function() {
 setTimeout(onReady, getBoilTime());
 };

 }
 Вот такой код должен ничего не выводить:

 var coffeeMachine = new CoffeeMachine(50000);
 coffeeMachine.waterAmount = 200;

 coffeeMachine.run();
 coffeeMachine.stop(); // кофе приготовлен не будет
 P.S. Текущую температуру воды вычислять и хранить не требуется.

 P.P.S. При решении вам, скорее всего, понадобится добавить приватное свойство timerId, которое будет хранить текущий таймер.
 */

function CoffeeMachine(power) {
    this.waterAmount = 0;

    const WATER_HEAT_CAPACITY = 4200;

    let self = this;
    let timerID = null;

    function getBoilTime() {
        return self.waterAmount * WATER_HEAT_CAPACITY * 80 / power;
    }

    function onReady() {
        alert( 'Кофе готово!' );
    }

    this.run = function() {
        self.timerID = setTimeout(onReady, getBoilTime());
    };

    this.stop = () => {
        clearTimeout(self.timerID);
    }

}
//
// let coffeeMachine = new CoffeeMachine(50000);
// coffeeMachine.waterAmount = 200;
//
// coffeeMachine.run();
// coffeeMachine.stop();