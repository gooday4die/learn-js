'use strict';

/*
 Функциональный паттерн наследования.

 Объявляется конструктор родителя Machine. В нём могут быть приватные (private), публичные (public) и защищённые (protected) свойства:

 function Machine(params) {
 // локальные переменные и функции доступны только внутри Machine
 let privateProperty;

 // публичные доступны снаружи
 this.publicProperty = ...;

 // защищённые доступны внутри Machine и для потомков
 // мы договариваемся не трогать их снаружи
 this._protectedProperty = ...
 }

 let machine = new Machine(...)
 machine.public();
 Для наследования конструктор потомка вызывает родителя в своём контексте через apply. После чего может добавить свои переменные и методы:

 function CoffeeMachine(params) {
 // универсальный вызов с передачей любых аргументов
 Machine.apply(this, arguments);

 this.coffeePublicProperty = ...
 }

 let coffeeMachine = new CoffeeMachine(...);
 coffeeMachine.publicProperty();
 coffeeMachine.coffeePublicProperty();
 В CoffeeMachine свойства, полученные от родителя, можно перезаписать своими. Но обычно требуется не заменить, а расширить метод родителя. Для этого он предварительно копируется в переменную:

 function CoffeeMachine(params) {
 Machine.apply(this, arguments);

 let parentProtected = this._protectedProperty;
 this._protectedProperty = function(args) {
 parentProtected.apply(this, args); // (*)
 // ...
 };
 }
 Строку (*) можно упростить до parentProtected(args), если метод родителя не использует this, а, например, привязан к  let self = this:

 function Machine(params) {
 let self = this;

 this._protected = function() {
 self.property = "value";
 };
 }
 Надо сказать, что способ наследования, описанный здесь - "так себе" если честно.

 Прототипный подход обладает рядом преимуществ.
 */

/*
 Задача 1: Запускать только при включённой кофеварке
 В коде CoffeeMachine сделайте так, чтобы метод run выводил ошибку, если кофеварка выключена.

 В итоге должен работать такой код:

 let coffeeMachine = new CoffeeMachine(10000);
 coffeeMachine.run(); // ошибка, кофеварка выключена!
 А вот так – всё в порядке:

 let coffeeMachine = new CoffeeMachine(10000);
 coffeeMachine.enable();
 coffeeMachine.run(); // ...Кофе готов!
 */

function Machine(power) {
    this._enabled = false;
    this._workProcessId = NaN;

    this.enable = function () {
        this._enabled = true;
    };

    this.disable = function () {

        clearTimeout(this._workProcessId);
        this._enabled = false;
    };
}

function CoffeeMachine(power) {
    Machine.apply(this, arguments);

    let waterAmount = 0;

    this.setWaterAmount = function (amount) {
        waterAmount = amount;
    };

    function onReady() {
        console.info('Кофе готово!');
    }

    this.run = function () {
        if (!this._enabled) throw new Error('Кофеварка выключена!');
        this._workProcessId = setTimeout(onReady, 1000);
    };

}
// Ответ
// let coffeeMachine = new CoffeeMachine(10000);
// coffeeMachine.run(); // ошибка, кофеварка выключена!
//
// coffeeMachine.enable();
// coffeeMachine.run(); // ...Кофе готов!

/*
 Задача 2: Останавливать кофеварку при выключении
 Когда кофеварку выключают – текущая варка кофе должна останавливаться.

 Например, следующий код кофе не сварит:

 var coffeeMachine = new CoffeeMachine(10000);
 coffeeMachine.enable();
 coffeeMachine.run();
 coffeeMachine.disable(); // остановит работу, ничего не выведет
 */

// let coffeeMachine = new CoffeeMachine(10000);
// coffeeMachine.enable();
// coffeeMachine.run();
// coffeeMachine.disable(); // остановит работу, ничего не выведет

/*
 Задача 3 : Унаследуйте холодильник
 Создайте класс для холодильника Fridge(power), наследующий от Machine, с приватным свойством food и методами addFood(...), getFood():

 Приватное свойство food хранит массив еды.
 Публичный метод addFood(item) добавляет в массив food новую еду, доступен вызов с несколькими аргументами addFood(item1, item2...) для добавления нескольких элементов сразу.
 Если холодильник выключен, то добавить еду нельзя, будет ошибка.
 Максимальное количество еды ограничено power/100, где power – мощность холодильника, указывается в конструкторе. При попытке добавить больше – будет ошибка
 Публичный метод getFood() возвращает еду в виде массива, добавление или удаление элементов из которого не должно влиять на свойство food холодильника.
 Код для проверки:

 var fridge = new Fridge(200);
 fridge.addFood("котлета"); // ошибка, холодильник выключен
 Ещё код для проверки:

 // создать холодильник мощностью 500 (не более 5 еды)
 var fridge = new Fridge(500);
 fridge.enable();
 fridge.addFood("котлета");
 fridge.addFood("сок", "зелень");
 fridge.addFood("варенье", "пирог", "торт"); // ошибка, слишком много еды
 Код использования холодильника без ошибок:

 var fridge = new Fridge(500);
 fridge.enable();
 fridge.addFood("котлета");
 fridge.addFood("сок", "варенье");

 var fridgeFood = fridge.getFood();
 alert( fridgeFood ); // котлета, сок, варенье

 // добавление элементов не влияет на еду в холодильнике
 fridgeFood.push("вилка", "ложка");

 alert( fridge.getFood() ); // внутри по-прежнему: котлета, сок, варенье
 Исходный код класса Machine, от которого нужно наследовать:

 function Machine(power) {
 this._power = power;
 this._enabled = false;

 var self = this;

 this.enable = function() {
 self._enabled = true;
 };

 this.disable = function() {
 self._enabled = false;
 };
 }
 */

function Machine(power) {
    this._enabled = false;
    this._workProcessId = NaN;

    this.enable = function () {
        this._enabled = true;
    };

    this.disable = function () {
        clearTimeout(this._workProcessId);
        this._enabled = false;
    };
}

function Fridge(power) {
    Machine.apply(this, arguments);

    const MAX_FIRDGE_SIZE = 5;

    let self = this;
    let parentDisable = this.disable;
    let _fridgeBag = [];

    this.disable = function () {
        if (_fridgeBag.length) throw new Error("В холодильнике есть еда!");
        parentDisable.call(this);
    };

    this.addFood = function (...foodItems) {
        try {
            if (checkFridgeInvite(...foodItems)) {
                _.forEach(foodItems, function (i) {
                    _fridgeBag.push(i);
                })
            }
        } catch (e) {
            console.error(e);
        }
    };

    this.getFood = function () {
        return _fridgeBag;
    };

    function checkFridgeInvite(...foodItems) {
        if (!arguments.length) {
            console.error("Воздух там уже есть");
            return false;
        }
        if ((_fridgeBag.length + countInputFood(...foodItems)) > MAX_FIRDGE_SIZE) throw new Error("Слишком много еды");

        return true;
    }

    function countInputFood(...foodItems) {
        if (arguments.length == MAX_FIRDGE_SIZE) {
            throw new Error(`Максимальная вместимость холодильника равна ${MAX_FIRDGE_SIZE}`)
        }
        return arguments.length;
    }

    this.filterFood = function (func) {
        return _.filter(_fridgeBag, func);

    }

    this.removeFood = function (item) {
        try {
            if (!_.isObject(item)) {
                throw new Error('Не еда!;')

            }
            return _fridgeBag.forEach(function (i, k) {
                if (item == i) {
                    _fridgeBag.splice(k, 1);
                }
            });
        } catch (e) {
            console.error(e);
        }
    }
}

/* Ответ 3
 var fridge = new Fridge(500);
 fridge.enable();
 fridge.addFood("котлета");
 fridge.addFood("сок", "варенье");
 fridge.addFood("сок", "варенье");
 fridge.addFood("сок");

 var fridgeFood = fridge.getFood();
 console.info(fridgeFood); // котлета, сок, варенье

 try {
 // добавление элементов не влияет на еду в холодильнике
 fridgeFood.push("вилка", "ложка");
 } catch (e) {
 console.error(e);
 }
 console.info(fridge.getFood()); // внутри по-прежнему: котлета, сок, варенье
 */

/*
 Задача 4: Добавьте методы в холодильник
 Добавьте в холодильник методы:

 Публичный метод filterFood(func), который возвращает всю еду, для которой func(item) == true
 Публичный метод removeFood(item), который удаляет еду item из холодильника.
 Код для проверки:

 */
/*
 Ответ 4
 var fridge = new Fridge(500);
 fridge.enable();
 fridge.addFood({
 title: "котлета",
 calories: 100
 });
 fridge.addFood({
 title: "сок",
 calories: 30
 });
 fridge.addFood({
 title: "зелень",
 calories: 10
 });
 fridge.addFood({
 title: "варенье",
 calories: 150
 });

 fridge.removeFood("нет такой еды"); // без эффекта
 console.log(fridge.getFood().length); // 4

 var dietItems = fridge.filterFood(function (item) {
 return item.calories < 50;
 });

 dietItems.forEach(function (item) {
 console.log(item.title); // сок, зелень
 fridge.removeFood(item);
 });

 console.log(fridge.getFood().length); // 2
 */

/*
 Задача 5: реопределите disable
 важность: 5решение
 Переопределите метод disable холодильника, чтобы при наличии в нём еды он выдавал ошибку.

 Код для проверки:

 */

var fridge = new Fridge(500);
fridge.enable();
fridge.addFood("кус-кус");
fridge.disable(); // ошибка, в холодильнике есть еда
