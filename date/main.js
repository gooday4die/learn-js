'use strict';
/*
 Дата и время

 - Дата и время представлены в JavaScript одним объектом: Date. Создать «только время» при этом нельзя, оно должно быть
 с датой. Список методов Date вы можете найти в справочнике Date или выше.
 - Отсчёт месяцев начинается с нуля.
 - Отсчёт дней недели (для getDay()) тоже начинается с нуля (и это воскресенье).
 - Объект Date удобен тем, что автокорректируется. Благодаря этому легко сдвигать даты.
 - При преобразовании к числу объект Date даёт количество миллисекунд, прошедших с 1 января 1970 UTC. Побочное
 следствие – даты можно вычитать, результатом будет разница в миллисекундах.
 - Для получения текущей даты в миллисекундах лучше использовать Date.now(), чтобы не создавать лишний объект Date
 (кроме IE8-)
 - Для бенчмаркинга лучше использовать performance.now() (кроме IE9-), он в 1000 раз точнее.
 */

/*
 Задача 1: Создайте дату
 Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут.

 Временная зона – местная. Выведите его на экран.
 */

// console.log(new Date(2012, 1, 20, 3, 12));

/*
 Задача 2: Имя дня недели
 Создайте функцию getWeekDay(date), которая выводит текущий день недели в коротком формате „пн“, „вт“, … „вс“.

 Например:
 var date = new Date(2012,0,3);  // 3 января 2012
 alert( getWeekDay(date) );      // Должно вывести 'вт'
 */
let task2 = (date) => date.toLocaleString('ru', {weekday: 'short'});

// console.log(task2(new Date()));

/*
 Задача 3: День недели в европейской нумерации
 Напишите функцию, getLocalDay(date) которая возвращает день недели для даты date.

 День нужно возвратить в европейской нумерации, т.е. понедельник имеет номер 1, вторник номер 2, …, воскресенье – номер 7.

 var date = new Date(2012, 0, 3);  // 3 янв 2012
 alert( getLocalDay(date) );       // вторник, выведет 2
 */
let task3 = (date) => {
    const sunday = 7;
    let day = date.getDay();

    return (day === 0 ) ? sunday : day;
};

// console.log(task3(new Date(2012, 0, 3)));

/*
 Задача 4: День указанное количество дней назад
 Создайте функцию getDateAgo(date, days), которая возвращает число, которое было days дней назад от даты date.

 Например, для 2 января 2015:

 var date = new Date(2015, 0, 2);

 alert( getDateAgo(date, 1) ); // 1, (1 января 2015)
 alert( getDateAgo(date, 2) ); // 31, (31 декабря 2014)
 alert( getDateAgo(date, 365) ); // 2, (2 января 2014)
 P.S. Важная деталь: в процессе вычислений функция не должна менять переданный ей объект date.
 */
let task4 = (date, days) => {
    let copyDate = new Date(date);

    copyDate.setDate(date.getDate() - days);

    return copyDate.getDate();
};

// console.log(task4(new Date(2015, 0, 2),3));

/*
 Задача 5: Последний день месяца?
 Напишите функцию getLastDayOfMonth(year, month), которая возвращает последний день месяца.

 Параметры:

 year – 4-значный год, например 2012.
 month – месяц от 0 до 11.
 Например, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль).
 */

let task5 = (year, month) => new Date(year, month + 1, 0).getDate();

// console.log(task5(2012,1));

/*

 Задача 6: Сколько секунд уже прошло сегодня?
 Напишите функцию getSecondsToday() которая возвращает, сколько секунд прошло с начала сегодняшнего дня.

 Например, если сейчас 10:00 и не было перехода на зимнее/летнее время, то:

 getSecondsToday() == 36000 // (3600 * 10)
 Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.
 */
let task6 = () => {
    let today = new Date();
    today.setHours(0, 0, 0, 0);

    return Math.floor((Date.now() - today) / 1000);
};

// console.log(task6());

/*
 Задача 7: Сколько секунд - до завтра?
 Напишите функцию getSecondsToTomorrow() которая возвращает, сколько секунд осталось до завтра.

 Например, если сейчас 23:00, то:

 getSecondsToTomorrow() == 3600
 P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного значения сегодняшней даты.
 */
let task7 = () => {
    let now = new Date();
    let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1);

    return Math.floor((tomorrow - now) / 1000);
};

// console.log(task7());

/*
 Задача 8: Вывести дату в формате дд.мм.гг
 Напишите функцию formatDate(date), которая выводит дату date в формате дд.мм.гг:

 Например:

 var d = new Date(2014, 0, 30); // 30 января 2014
 alert( formatDate(d) ); // '30.01.14'
 P.S. Обратите внимание, ведущие нули должны присутствовать, то есть 1 января 2001 должно быть 01.01.01, а не 1.1.1.
 */

let task8 = (date) => {
    let formatter = new Intl.DateTimeFormat("ru", {
        year: "2-digit",
        month: "2-digit",
        day: "2-digit"
    });

    return formatter.format(date);
};

// console.log(task8(new Date(2014, 11, 31, 12, 30, 0)));

/*
 Задача 9: Относительное форматирование даты
 Напишите функцию formatDate(date), которая форматирует дату date так:

 Если со времени date прошло менее секунды, то возвращает "только что".
 Иначе если со времени date прошло менее минуты, то "n сек. назад".
 Иначе если прошло меньше часа, то "m мин. назад".
 Иначе полная дата в формате "дд.мм.гг чч:мм".
 Например:

 function formatDate(date) { // ваш код  }

 alert( formatDate(new Date(new Date - 1)) ); // "только что"

 alert( formatDate(new Date(new Date - 30 * 1000)) ); // "30 сек. назад"

 alert( formatDate(new Date(new Date - 5 * 60 * 1000)) ); // "5 мин. назад"

 alert( formatDate(new Date(new Date - 86400 * 1000)) ); // вчерашняя дата в формате "дд.мм.гг чч:мм"
 */

let task9 = (date) => {
    let diff = new Date - date;
    let formatter = new Intl.DateTimeFormat("ru", {
        year: "2-digit",
        month: "2-digit",
        day: "2-digit",
        hour: "numeric",
        minute: "numeric",
    });

    switch (diff) {
        case 1:
            return "тольцо что";
        case 30 * 1000:
            return "30 сек. назад";
        case  5 * 60 * 1000:
            return "5 мин. назад";
        default:
            return formatter.format(date);
    }
};

// console.log(task9(new Date(new Date - 1))); // "только что"
// console.log(task9(new Date(new Date - 30 * 1000))); // "30 сек. назад"
// console.log(task9(new Date(new Date - 5 * 60 * 1000))); // "5 мин. назад"
// console.log(task9(new Date(new Date - 86400 * 1000))); // вчерашняя дата в формате "дд.мм.гг чч:мм"

